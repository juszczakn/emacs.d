;; Customize this per work env, as needed. Default.

;; Misc

(defvar my-work-packages
  '(clojure-mode
    cider
    racket-mode
    slime
    lsp-mode
    tree-sitter
    tree-sitter-langs))

(dolist (p my-work-packages)
  (when (not (package-installed-p p))
    (package-install p)))


(setq split-height-threshold 110
      split-width-threshold 200)


;;Turn on paredit mode in clojure-mode
(defun turn-on-paredit () (paredit-mode 1))
(add-hook 'clojure-mode-hook 'turn-on-paredit)
(add-hook 'lisp-mode-hook 'turn-on-paredit)
(add-hook 'emacs-lisp-mode-hook 'turn-on-paredit)
(add-hook 'racket-mode-hook 'turn-on-paredit)


;; Clojure

(add-to-list 'load-path "~/.emacs.d/manual-packages/cider")
(require 'cider)


;; Racket

(require 'racket-xp)
(add-hook 'racket-mode-hook #'racket-xp-mode)

(setq racket-program (substitute-in-file-name "/home/linuxbrew/.linuxbrew/bin/racket"))

(add-to-list 'auto-mode-alist '("\\.rkt\\'" . racket-mode))
(defun my-racket-xp-describe ()
  (interactive)
  (racket-xp-describe)
  (ace-window 1))
(add-hook 'racket-xp-mode-hook
          (lambda ()
            ;;(remove-hook 'pre-redisplay-functions #'racket-xp-pre-redisplay t)
            (define-key racket-xp-mode-map (kbd "C-c C-.") 'my-racket-xp-describe)))
(setq racket-xp-after-change-refresh-delay 5)
(setq racket-memory-limit 2000)


;; SQL

(eval-after-load "sql"
  '(load-library "sql-indent"))


(defun my-sql-mode-hook ()
  (custom-set-variables
   '(tab-width 2)))

(add-hook 'sql-mode-hook #'my-sql-mode-hook)

(setq lsp-keymap-prefix "s-l")
(require 'lsp-mode)
(add-hook 'typescript-ts-mode-hook #'lsp)


(defun seq-keep (function sequence)
  "Apply FUNCTION to SEQUENCE and return the list of all the non-nil results."
  (delq nil (seq-map function sequence)))


(setq treesit-language-source-alist
      '((bash "https://github.com/tree-sitter/tree-sitter-bash")
        (cmake "https://github.com/uyha/tree-sitter-cmake")
        (css "https://github.com/tree-sitter/tree-sitter-css")
        (elisp "https://github.com/Wilfred/tree-sitter-elisp")
        (go "https://github.com/tree-sitter/tree-sitter-go")
        (html "https://github.com/tree-sitter/tree-sitter-html")
        (javascript "https://github.com/tree-sitter/tree-sitter-javascript" "master" "src")
        (json "https://github.com/tree-sitter/tree-sitter-json")
        (make "https://github.com/alemuller/tree-sitter-make")
        (markdown "https://github.com/ikatyang/tree-sitter-markdown")
        (python "https://github.com/tree-sitter/tree-sitter-python")
        (toml "https://github.com/tree-sitter/tree-sitter-toml")
        (tsx "https://github.com/tree-sitter/tree-sitter-typescript" "master" "tsx/src")
        (typescript "https://github.com/tree-sitter/tree-sitter-typescript" "master" "typescript/src")
        (yaml "https://github.com/ikatyang/tree-sitter-yaml")))

(setq exec-path (cons "/home/linuxbrew/.linuxbrew/bin" exec-path))
