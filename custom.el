(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(blink-cursor-mode nil)
 '(c-basic-offset 4)
 '(c-default-style "linux")
 '(c-offsets-alist '((inline-open . +)))
 '(custom-enabled-themes '(wombat))
 '(delete-selection-mode nil)
 '(git-gutter:handled-backends '(git hg))
 '(global-linum-mode t)
 '(inhibit-startup-screen t)
 '(mark-even-if-inactive t)
 '(package-selected-packages
   '(tree-sitter-langs tree-sitter ivy-rich projectile git-gutter dash counsel ivy cider lsp-mode rustic markdown-mode racket-mode merlin-eldoc tuareg smex slime s paredit ocp-indent monky merlin magit json-reformat git-gutter-fringe company clojure-mode buffer-move autopair ace-window))
 '(scroll-bar-mode nil)
 '(show-paren-mode t)
 '(tab-width 4)
 '(tool-bar-mode nil)
 '(transient-mark-mode 1))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "DejaVu Sans Mono" :foundry "unknown" :slant normal :weight normal :height 98 :width normal))))
 '(linum ((t (:inherit (shadow default) :foreground "medium slate blue")))))
(put 'downcase-region 'disabled nil)
