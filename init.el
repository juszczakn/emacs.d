(setq custom-file "~/.emacs.d/custom.el")
(load custom-file)

(require 'package)
(setq package-archives
      '(("melpa" . "https://melpa.org/packages/")
        ("elpa" . "https://elpa.gnu.org/packages/")))
(package-initialize)

(when (not package-archive-contents)
  (package-refresh-contents))

(defvar my-packages
  '(s
    dash company
    paredit
    ;;autopair
    magit
    git-gutter git-gutter-fringe
    json-reformat
    smex
    ace-window
    ivy
    counsel
    swiper
    projectile ivy-rich))

(dolist (p my-packages)
  (when (not (package-installed-p p))
    (package-install p)))

(require 's)
(require 'dash)

(global-set-key (kbd "C-c m") 'magit-status)

(global-unset-key (kbd "C-z"))

;; auto indent code pasted into emacs
(dolist (command '(yank yank-pop))
  (eval `(defadvice ,command (after indent-region activate)
           (and (not current-prefix-arg)
                (member major-mode '(emacs-lisp-mode
                                     lisp-mode
                                     clojure-mode scheme-mode
                                     haskell-mode ruby-mode
                                     rspec-mode python-mode
                                     c-mode c++-mode
                                     objc-mode latex-mode
                                     plain-tex-mode))
                (let ((mark-even-if-inactive transient-mark-mode))
                  (indent-region (region-beginning) (region-end) nil))))))


;;Stop emacs from jumping around when scrolling
(setf scroll-step 1)
(setf scroll-conservatively 9001)

(global-display-line-numbers-mode)

;;Open new files in current emacs instance
(require 'server)
(if (and (fboundp 'server-running-p)
         (not (server-running-p)))
    (server-start))

;;Title for frame
(setf frame-title-format "%b -- %f")

(menu-bar-mode -1)

;; trying ivy instead of ido
(ivy-mode 1)
(global-set-key (kbd "M-x") 'counsel-M-x)
(global-set-key (kbd "C-x C-f") 'counsel-find-file)
(setq ivy-use-virtual-buffers t)
(setq ivy-count-format "(%d/%d) ")

(global-set-key (kbd "C-s") 'swiper-isearch)
(global-set-key (kbd "C-r") 'swiper-isearch-backward)

(defun my-refresh-buffer ()
  (interactive)
  (revert-buffer t t))

(global-set-key [f5] 'my-refresh-buffer)
(global-set-key [f6] 'apropos)


;; More standard-like C-backspace
(defun my-kill-back ()
  (interactive)
  (if (bolp)
      (backward-delete-char 1)
    (if (string-match "[^[:space:]]" (buffer-substring (point-at-bol) (point)))
        (backward-kill-word 1)
      (delete-region (point-at-bol) (point)))))

(global-set-key [C-backspace] 'my-kill-back)

;;c-x p to previous window
(global-set-key "\C-xp" #'(lambda () (interactive) (other-window -1)))

(setf c-default-style
      '((java-mode . "linux")
        (awk-mode . "awk")
        (other . "linux")))

(when (window-system)
  (require 'git-gutter-fringe))

(global-git-gutter-mode +1)
(setq-default indicate-buffer-boundaries 'left)
(setq-default indicate-empty-lines +1)

(setf mouse-wheel-follow-mouse 't)
(setf mouse-wheel-scroll-amount '(1 ((shift) . 1)))

(require 'uniquify)
(setf uniquify-buffer-name-style 'forward)


;; (require 'autopair)
;; (autopair-global-mode)

(setq-default indent-tabs-mode nil)

;;(global-hl-line-mode)

(defun show-file-path ()
  "Show the full path file name in the minibuffer."
  (interactive)
  (kill-new (buffer-file-name))
  (message (buffer-file-name)))
(defun show-file-name ()
  (interactive)
  (kill-new (buffer-name))
  (message (buffer-name)))


(defun my-next-git-gutter-diff (arg)
  (interactive "p")
  (git-gutter:next-diff arg)
  (recenter))
(defun my-prev-git-gutter-diff (arg)
  (interactive "p")
  (git-gutter:previous-diff arg)
  (recenter))
(global-set-key (kbd "C-s-n") 'my-next-git-gutter-diff)
(global-set-key (kbd "C-s-p") 'my-prev-git-gutter-diff)


(defalias 'ff 'find-file)
(defalias 'ffw 'find-file-other-window)

(defun eshell-clear-buffer ()
  "Clear terminal"
  (interactive)
  (let ((inhibit-read-only t))
    (erase-buffer)
    (eshell-send-input)))

(global-set-key (kbd "C-x o") 'ace-window)
(setq aw-keys '(?a ?s ?d ?f ?g ?h ?j ?k ?l))

(defun insert-date-time ()
  "Insert current date-time string in full ISO 8601 format.
Example: 2010-11-29T23:23:35-08:00"
  (interactive)
  (insert
   (concat
    (format-time-string "%Y-%m-%d %T")
    ((lambda (x) (concat (substring x 0 3) ":" (substring x 3 5)))
     (format-time-string "%z")))))


(defun open-new-tmp-file ()
  (interactive)
  (switch-to-buffer
   (concat "*tmp-file-" (symbol-name (gensym)) "*")))

(global-unset-key (kbd "C-x C-c"))
(global-set-key (kbd "C-x C-c C-c") #'save-buffers-kill-terminal)

(setq visible-bell 1)

;; Put backup files neatly away
(let ((backup-dir "~/tmp/emacs/backups")
      (auto-saves-dir "~/tmp/emacs/auto-saves/"))
  (dolist (dir (list backup-dir auto-saves-dir))
    (when (not (file-directory-p dir))
      (make-directory dir t)))
  (setq backup-directory-alist `(("." . ,backup-dir))
        auto-save-file-name-transforms `((".*" ,auto-saves-dir t))
        auto-save-list-file-prefix (concat auto-saves-dir ".saves-")
        tramp-backup-directory-alist `((".*" . ,backup-dir))
        tramp-auto-save-directory auto-saves-dir))

(setq backup-by-copying t    ; Don't delink hardlinks
      delete-old-versions t  ; Clean up the backups
      version-control t      ; Use version numbers on backups,
      kept-new-versions 5    ; keep some new versions
      kept-old-versions 2)   ; and some old ones, too


(defun init ()
  (interactive)
  (find-file "~/.emacs.d/init.el"))

(projectile-mode +1)
;; Recommended keymap prefix on Windows/Linux
(define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)


;;;; Work specific

(load "~/.emacs.d/work.el")
